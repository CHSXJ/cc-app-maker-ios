//
//  FindMusicAroundFlowCoordinator.swift
//  CCAPPMAKER
//
//  Created by Cherry on 10/27/20.
//

import UIKit

protocol FindMusicAroundFlowCoordinatorDependencies {
    func makeMusicAroundMeViewController(closures: ActiveMusicAroundViewModelClosures) -> ActiveMusicAroundViewController
}

class FindMusicAroundFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: FindMusicAroundFlowCoordinatorDependencies
    
    private weak var findMusicAroundVC: ActiveMusicAroundViewController?
    
    init(navigationController: UINavigationController,
         dependencies: FindMusicAroundFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        // Note: here we keep strong reference with closures, this way this flow do not need to be strong referenced
        let closures = ActiveMusicAroundViewModelClosures()
        let vc = dependencies.makeMusicAroundMeViewController(closures: closures)

        navigationController?.pushViewController(vc, animated: false)
        findMusicAroundVC = vc
    }
    
}
