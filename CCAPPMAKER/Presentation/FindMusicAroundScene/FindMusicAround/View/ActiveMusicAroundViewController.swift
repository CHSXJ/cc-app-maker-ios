//
//  ActiveMusicAroundViewController.swift
//  CCAPPMAKER
//
//  Created by Cherry on 10/27/20.
//

import UIKit

class ActiveMusicAroundViewController: UIViewController, StoryboardInstantiable {
    
    private var viewModel: ActiveMusicAroundViewModel!
    
    static func create(with viewModel: ActiveMusicAroundViewModel) -> ActiveMusicAroundViewController {
        let view = ActiveMusicAroundViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }
    
}
