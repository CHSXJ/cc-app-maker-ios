//
//  ActiveMusicAroundViewModel.swift
//  CCAPPMAKER
//
//  Created by Cherry on 10/27/20.
//

import Foundation

struct ActiveMusicAroundViewModelClosures {
    /// Note: if you would need to edit movie inside Details screen and update this Movies List screen with updated movie then you would need this closure:
    /// showMovieDetails: (Movie, @escaping (_ updated: Movie) -> Void) -> Void
//    let showMusicAround: (ActiveMusic) -> Void
}

protocol MusicAroundViewModelInput {
}

protocol MusicAroundViewModelOutput {}

protocol MusicAroundViewModel: MusicAroundViewModelInput, MusicAroundViewModelOutput {}

final class ActiveMusicAroundViewModel: MusicAroundViewModel {
    
    // MARK: - Init

    init() { }
}
