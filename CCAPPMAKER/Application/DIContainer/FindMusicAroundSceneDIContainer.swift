//
//  FindMusicAroundSceneDIContainer.swift
//  CCAPPMAKER
//
//  Created by Cherry on 10/27/20.
//

import UIKit
import SwiftUI

final class FindMusicAroundSceneDIContainer {
    
    struct Dependencies { }
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - Flow Coordinators
    func makeFindMusicAroundFlowCoordinator(navigationController: UINavigationController) -> FindMusicAroundFlowCoordinator {
        return FindMusicAroundFlowCoordinator(navigationController: navigationController,
                                           dependencies: self)
    }
    
    // MARK: - Active Music Around
    func makeMusicAroundMeViewController(closures: ActiveMusicAroundViewModelClosures) -> ActiveMusicAroundViewController {
        return ActiveMusicAroundViewController.create(with: makeActiveMusicAroundViewModel(closures: closures))
    }
    
    func makeActiveMusicAroundViewModel(closures: ActiveMusicAroundViewModelClosures) -> ActiveMusicAroundViewModel {
        return ActiveMusicAroundViewModel()
    }
    
}

extension FindMusicAroundSceneDIContainer: FindMusicAroundFlowCoordinatorDependencies { }
